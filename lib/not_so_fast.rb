# frozen_string_literal: true
require 'net/http'
require 'json'

require_relative "not_so_fast/version"

module NotSoFast
  class Error < StandardError; end

  def self.get(url)
    uri = URI(url)
    Net::HTTP.start(uri.host, uri.port, use_ssl: uri.scheme == "https") do |http|
      request = Net::HTTP::Get.new uri
      request["user-agent"] = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/112.0"
      response = http.request request
      if response.is_a?(Net::HTTPSuccess)
        return response.body
      else
        puts "FAILED to fetch #{url}, response '#{response.message}'"
        puts "RESPONSE HEADERS:"
        puts response.to_hash.inspect
        puts "REQUEST HEADERS:"
        puts request.to_hash.inspect
        return nil
      end
    end
  rescue Errno::ECONNRESET
    puts "FAILED GET #{url}: Connection reset by peer"
    return nil
  end

  #
  # returns speed in bps
  #
  def self.fetch_payload(payload_url, seconds_to_run)
    start_time = Time.now
    uri = URI(payload_url)
    speed = 0
    size = 0
    Net::HTTP.start(uri.host, uri.port, use_ssl: uri.scheme == "https") do |http|
      http.request_get(uri) do |resp|
        resp.read_body do |segment|
          time_spent = Time.now - start_time
          size += segment.bytesize * 8
          speed = size / time_spent
          if time_spent >= seconds_to_run
            return speed
          end
        end
      end
    end
    return speed
  end

  def self.run(seconds_to_run=10)
    fast_dot_com = get("https://fast.com")
    return 0 unless fast_dot_com
    app_url = "https://fast.com" + /(\/app-[[:alnum:]]+\.js)/.match(fast_dot_com)[1]
    app_javascript = get(app_url)
    return 0 unless app_javascript
    token = /token:"([[:alnum:]]*)"/.match(app_javascript)&.[](1)
    unless token
      puts "ERROR: failed to parse token from javascript"
      return 0
    end
    api_json = get("https://api.fast.com/netflix/speedtest?https=true&token=#{token}&urlCount=1")
    return 0 unless api_json
    begin
      payload_url = JSON.parse(api_json)[0]["url"]
    rescue StandardError => exc
      puts "ERROR: failed to parse JSON from fast.com"
      puts api_json
      puts exc.to_s
      return 0
    end
    speed = fetch_payload(payload_url, seconds_to_run)
    return speed.round
  end
end
