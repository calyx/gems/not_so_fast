# frozen_string_literal: true

require_relative "lib/not_so_fast/version"

Gem::Specification.new do |spec|
  spec.name = "not_so_fast"
  spec.version = NotSoFast::VERSION
  spec.authors = ["ziggy"]
  spec.email = ["ziggy@calyxinstitute.org"]

  spec.summary = "A simple fast.com client"
  spec.homepage = "https://0xacab.org/calyx/gems/not_so_fast.git"
  spec.license = "MIT"

  spec.metadata["allowed_push_host"] = "TODO: Set to your gem server 'https://example.com'"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = spec.homepage

  spec.files = Dir["exe/not_so_fast"] + Dir["lib/**/*.rb"] + ["Gemfile", "README.md", "LICENSE.txt"]
  spec.bindir = "exe"
  spec.executables = "not_so_fast"
  spec.require_paths = ["lib"]
end
