# NotSoFast

A simple fast.com client

## Installation

Install the gem and add to the application's Gemfile by executing:

    $ bundle add not_so_fast

If bundler is not being used to manage dependencies, install the gem by executing:

    $ gem install not_so_fast

## Usage


Running `not_so_fast` will run display your download speed. From ruby use `NotSoFast.run`. The speed is measured in bps.


## Development

After checking out the repo, run `bin/setup` to install dependencies. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome at https://0xacab.org/calyx/gems/not_so_fast.git

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
